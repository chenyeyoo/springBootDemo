package com.demo.thread;

/**
 * @Package: [com.demo.thread]
 * @Description: [一句话描述该类的功能]
 * @Author: [chenye]
 * @CreateDate: [2018/1/8 15:54]
 * @UpdateUser: [chenye(如多次修改保留历史记录，增加修改记录)]
 * @UpdateDate: [2018/1/8 15:54，(如多次修改保留历史记录，增加修改记录)]
 * @UpdateRemark: [说明本次修改内容, (如多次修改保留历史记录，增加修改记录)]
 * @Version: [v1.0]
 */
public class ExceptionHandlerThread implements Thread.UncaughtExceptionHandler{
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        System.out.println("threadName"+t.getName()+"--------threadId"+t.getId());
        e.printStackTrace();
    }
}
