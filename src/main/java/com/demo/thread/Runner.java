package com.demo.thread;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;

/**
 * @Package: [com.demo.thread]
 * @Description: [一句话描述该类的功能]
 * @Author: [chenye]
 * @CreateDate: [2018/1/8 15:53]
 * @UpdateUser: [chenye(如多次修改保留历史记录，增加修改记录)]
 * @UpdateDate: [2018/1/8 15:53，(如多次修改保留历史记录，增加修改记录)]
 * @UpdateRemark: [说明本次修改内容, (如多次修改保留历史记录，增加修改记录)]
 * @Version: [v1.0]
 */
public class Runner implements Callable {
    // 一个同步辅助类，它允许一组线程互相等待，直到到达某个公共屏障点 (common barrier point)
    private CyclicBarrier barrier;

    private String name;

    public Runner(CyclicBarrier barrier, String name) {
        super();
        this.barrier = barrier;
        this.name = name;
    }

    @Override
    public Object call() throws Exception {
        System.out.println(name + " 准备好了...");

        if(name.equals("3号")){
            try{
                Thread.sleep(3000);
                throw new RuntimeException("hahahahahha 1号出异常了");
            }catch (Exception e){
                //可将异常抛出处理
                //也可以在这里处理异常并赋予相应的返回值
                throw new RuntimeException("hahahahahha 1号出异常了");
            }finally {
//                barrier.await();
            }
        }else{
//            barrier.await();
        }
//            if(name.equals("2号")){
//                Thread.sleep(3000);
//            }
        // barrier的await方法，在所有参与者都已经在此 barrier 上调用 await 方法之前，将一直等待。
        return name + " 起跑！";
    }

}

