package com.demo.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ThreadFactory;

/**
 * @Package: [com.demo.thread]
 * @Description: [一句话描述该类的功能]
 * @Author: [chenye]
 * @CreateDate: [2018/1/8 15:57]
 * @UpdateUser: [chenye(如多次修改保留历史记录，增加修改记录)]
 * @UpdateDate: [2018/1/8 15:57，(如多次修改保留历史记录，增加修改记录)]
 * @UpdateRemark: [说明本次修改内容, (如多次修改保留历史记录，增加修改记录)]
 * @Version: [v1.0]
 */
public class MyThreadFactory implements ThreadFactory {
    private final ExceptionHandlerThread exceptionHandlerThread = new ExceptionHandlerThread();
    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r);
        thread.setUncaughtExceptionHandler(exceptionHandlerThread);
        return thread;
    }
    Thread thread = new Thread();
}
