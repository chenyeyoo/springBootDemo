package com.demo.thread;

import sun.misc.Unsafe;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Package: [com.demo.thread]
 * @Description: [一句话描述该类的功能]
 * @Author: [chenye]
 * @CreateDate: [2018/1/19 11:28]
 * @UpdateUser: [chenye(如多次修改保留历史记录，增加修改记录)]
 * @UpdateDate: [2018/1/19 11:28，(如多次修改保留历史记录，增加修改记录)]
 * @UpdateRemark: [说明本次修改内容, (如多次修改保留历史记录，增加修改记录)]
 * @Version: [v1.0]
 */
public class TestThread {


    private static class ServiceThread implements Callable{
        private static AtomicInteger num = new AtomicInteger(0);
        private static Integer num1 = 0;
        @Override
        public Object call() throws Exception {
//            System.out.println(Thread.currentThread().getName()+" startInt:"+num.get());
            num.compareAndSet(num.get(),num.intValue()+20);
//            System.out.println(Thread.currentThread().getName()+" endInt:"+num.get());
//            synchronized (num1){
//                num1++;
//                System.out.println(num1);
//            }
            return null;
        }
        public static void main(String[] args){
            ExecutorService executorService = Executors.newFixedThreadPool(10);
            long startTime = System.currentTimeMillis();
            System.out.println(startTime);
            for(int i=0;i<10000;i++){
                executorService.submit(new ServiceThread());
            }
            System.out.println("time"+(System.currentTimeMillis()-startTime));
            executorService.shutdown();
        }
    }
}
