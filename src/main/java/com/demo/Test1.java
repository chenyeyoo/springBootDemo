package com.demo;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Package: [com.demo]
 * @Description: [一句话描述该类的功能]
 * @Author: [chenye]
 * @CreateDate: [2018/2/26 9:31]
 * @UpdateUser: [chenye(如多次修改保留历史记录，增加修改记录)]
 * @UpdateDate: [2018/2/26 9:31，(如多次修改保留历史记录，增加修改记录)]
 * @UpdateRemark: [说明本次修改内容, (如多次修改保留历史记录，增加修改记录)]
 * @Version: [v1.0]
 */
public class Test1 {
    static final ReentrantLock reentrantLock = new ReentrantLock();
    static final Condition c = reentrantLock.newCondition();
    static final Object obj = new Object();
    public static final void begin()throws Exception{
//        try {
//            reentrantLock.lockInterruptibly();
//            System.out.println("开始阻塞");
            c.await();
            c.signal();
//            System.out.println("线程被唤醒了");
            synchronized (obj){
                System.out.println("开始阻塞");
                obj.wait();
                System.out.println("线程被唤醒了");
            }

//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }finally {
////            reentrantLock.unlock();
//        }
    }
    public static void main(String[] args)throws Exception{

        Thread thread = new Thread(new Worker(),"Test Thread 1");
        thread.start();
//        Thread.sleep(200);
        thread.interrupt();
//        Thread.currentThread().interrupt();




//        try {
//            reentrantLock.lockInterruptibly();
//            c.signal();
//            System.out.println("唤醒阻塞的线程");
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }finally {
//            reentrantLock.unlock();
//        }
//        thread.interrupt();
        Thread.sleep(3000);
    }
}
class Worker implements Runnable{
    @Override
    public void run() {
        System.out.println(Thread.currentThread().isInterrupted());
        System.out.println(Thread.interrupted());
        System.out.println(Thread.interrupted());
//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            Thread.currentThread().interrupt();
//            System.out.println("`1`112`12`1"+Thread.currentThread().isInterrupted());
////            System.out.println(Thread.interrupted());
////            System.out.println(Thread.interrupted());
////            System.out.println(Thread.currentThread().isInterrupted());
//        }
        for (int i = 0; i <100 ; i++) {
            i+=i;
        }

    }
}
class Father{
    public final static int num = 2;
    static{
        System.out.println("Father");
    }
}
class Child extends Father{
    public static int num = 1;
    static{
        System.out.println("Father");
    }
}