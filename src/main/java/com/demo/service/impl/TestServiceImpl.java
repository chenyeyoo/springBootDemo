package com.demo.service.impl;

import com.demo.domain.entity.User;
import com.demo.mapper.TestMapper;
import com.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @Package: [com.demo.service.impl]
 * @Description: [一句话描述该类的功能]
 * @Author: [chenye]
 * @CreateDate: [2018/3/9 11:08]
 * @UpdateUser: [chenye(如多次修改保留历史记录，增加修改记录)]
 * @UpdateDate: [2018/3/9 11:08，(如多次修改保留历史记录，增加修改记录)]
 * @UpdateRemark: [说明本次修改内容, (如多次修改保留历史记录，增加修改记录)]
 * @Version: [v1.0]
 */
@Service
public class TestServiceImpl implements TestService{
    @Autowired
    private TestMapper testMapper;
    @Override
//    @Transactional
    public void testCheck() {
        String name = "test";
        User user = new User();
        user.setName(name);
        user.setAge(22);
        user.setSex("男");
        user.setId(UUID.randomUUID().toString());
        testMapper.insert(user);
        user.setId(null);
        testMapper.insert(user);
        List<User> list = testMapper.selectByName(name);
        if(list.size()==3){
            throw new RuntimeException("already have");
        }
        System.out.println(list.size());
    }
}
