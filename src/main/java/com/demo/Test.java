package com.demo;


import sun.misc.Unsafe;
import sun.reflect.Reflection;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Package: [com.demo]
 * @Description: [一句话描述该类的功能]
 * @Author: [chenye]
 * @CreateDate: [2018/2/2 14:56]
 * @UpdateUser: [chenye(如多次修改保留历史记录，增加修改记录)]
 * @UpdateDate: [2018/2/2 14:56，(如多次修改保留历史记录，增加修改记录)]
 * @UpdateRemark: [说明本次修改内容, (如多次修改保留历史记录，增加修改记录)]
 * @Version: [v1.0]
 */
public class Test {
    public static void main(String args[])throws Exception{
        LinkedBlockingQueue b = new LinkedBlockingQueue(10);
        b.take();
        b.put(123);
        Thread.currentThread().isInterrupted();
//        CountDownLatch c = new CountDownLatch(4);
//        CyclicBarrier cb = new CyclicBarrier(3);
////        new Thread(new Boss(cb)).start();
//        for (int i = 0; i <3 ; i++) {
//            Thread t = new Thread(new Worker(cb));
//            t.setName(String.valueOf(i));
//            t.start();
//        }
//        System.out.println(Reflection.getCallerClass(2).getClassLoader());

//        ExecutorService e = Executors.newFixedThreadPool(4);
//        List<Future<Integer>> list = new ArrayList<>();
//        for (int i = 0; i <4 ; i++) {
//            list.add(e.submit(new Funny(i)));
//        }
//        e.shutdown();
//        for (int i = 0,size=list.size(); i < size; i++) {
//            list.get(i);
//            System.out.println(list.get(i).get());
//        }
//        ThreadPoolExecutor t = new ThreadPoolExecutor(1,1,0L,TimeUnit.MILLISECONDS,new SynchronousQueue());
//
//        LinkedList l = new LinkedList();
//        ArrayBlockingQueue arrayBq = new ArrayBlockingQueue(50);
//        LinkedBlockingQueue linkedBQ = new LinkedBlockingQueue();
//        SynchronousQueue sQ = new SynchronousQueue();
//        PriorityBlockingQueue priBQ = new PriorityBlockingQueue();
//        final ReentrantLock reentrantLock = new ReentrantLock();
//        final Condition c = reentrantLock.newCondition();
//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                try {
//                    reentrantLock.lockInterruptibly();
//                    System.out.println("开始阻塞");
//                    c.await();
//                    System.out.println("线程被唤醒了");
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }finally {
//                    reentrantLock.unlock();
//                }
//            }
//        },"Test Thread 1");
//        thread.start();
//        Thread.sleep(1000);
//        try {
//            reentrantLock.lockInterruptibly();
//            c.signal();
//            System.out.println("唤醒阻塞的线程");
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }finally {
//            reentrantLock.unlock();
//        }
//        thread.interrupt();
//        Thread.sleep(3000);
//        final Object obj = new Object();
//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("运行");
////                synchronized (obj){
////                    try {
////                        obj.wait();
////                    } catch (InterruptedException e) {
////                        e.printStackTrace();
////                    }
////                }
//                for (int i = 0; i < 100000; i++) {
////                    System.out.println(i);
//                }
//                System.out.println("线程被中断了");
//            }
//        });
//        thread.start();
////        Thread.sleep(10);
//        thread.interrupt();
//        System.out.println("调用线程中断了");

        Thread d = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("准备挂起线程");
                LockSupport.park();
                System.out.println("中断成功");
            }
        });
        d.start();
        Thread.sleep(1000);
        System.out.println("准备中断");
        d.interrupt();
    }
}
class Funny implements Callable{
    private int num;

    public Funny(int num) {
        this.num = num;
    }

    @Override
    public Object call() {
        if(num==1){
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return num;
    }
}
class Worker1 implements Runnable{
    private CyclicBarrier count;

    public Worker1(CyclicBarrier count) {
        this.count = count;
    }
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"号工人开始干活");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            count.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"号工人干完活");
    }
}

class Boss implements Runnable{
    private CyclicBarrier count;

    public Boss(CyclicBarrier count) {
        this.count = count;
    }

    @Override
    public void run() {
        System.out.println("boss来了");
        try {
            count.await();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("boss检查完工作了");
    }
}