package com.demo.config;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @Package: [com.demo.config]
 * @Description: [一句话描述该类的功能]
 * @Author: [chenye]
 * @CreateDate: [2018/3/8 9:21]
 * @UpdateUser: [chenye(如多次修改保留历史记录，增加修改记录)]
 * @UpdateDate: [2018/3/8 9:21，(如多次修改保留历史记录，增加修改记录)]
 * @UpdateRemark: [说明本次修改内容, (如多次修改保留历史记录，增加修改记录)]
 * @Version: [v1.0]
 */
@Aspect
@Component
public class TestAspect {
    @Pointcut("@annotation(Test)")
    public void point(){}

    @Before(value = "point()")
    public void before(){
        System.out.println("before");
    }
    @After(value = "point()")
    public void after(){
        System.out.println("after");
    }
}
