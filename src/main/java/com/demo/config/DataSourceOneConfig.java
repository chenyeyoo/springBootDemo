package com.demo.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.baomidou.mybatisplus.entity.GlobalConfiguration;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.interceptor.TransactionInterceptor;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Administrator on 2018/1/15.
 */
@Configuration
@MapperScan(basePackages = {"com.demo.mapper"},sqlSessionTemplateRef = "SqlSessionTemplateOne")
public class DataSourceOneConfig {

    @ConfigurationProperties(prefix = "spring.dataSource.druid.datasource1")
    @Bean(name = "datasource1")
    @Primary
    public DataSource dataSource1() throws SQLException {
        return DruidDataSourceBuilder.create().build();
    }
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }


    @Bean(name = "sessionFactory1")
    @Primary
    public SqlSessionFactory sqlSessionFactory1(@Qualifier(value = "datasource1") DataSource dataSource,
                                                PaginationInterceptor paginationInterceptor,
                                                @Qualifier(value = "globalConfiguration1") GlobalConfiguration globalConfiguration) throws Exception {
        MybatisSqlSessionFactoryBean bean = new MybatisSqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml"));
        bean.setGlobalConfig(globalConfiguration);
        Interceptor[] interceptors = new Interceptor[]{paginationInterceptor};
        bean.setPlugins(interceptors);
        return bean.getObject();
    }

    @ConfigurationProperties(prefix = "globalConfig1")
    @Bean(name = "globalConfiguration1")
    public GlobalConfiguration globalConfiguration1() {
        return new GlobalConfiguration();
    }


    @Bean(name = "transactionManager1")
    @Primary
    public DataSourceTransactionManager dataSourceTransactionManager1(@Qualifier("datasource1") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "SqlSessionTemplateOne")
    @Primary
    public SqlSessionTemplate testSqlSessionTemplate(@Qualifier("sessionFactory1") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

//    // 创建事务通知
//    @Bean(name = "txAdvice")
//    public TransactionInterceptor getAdvisor(@Qualifier("transactionManager1") DataSourceTransactionManager transactionManager) throws Exception {
//        Properties properties = new Properties();
//        properties.setProperty("get*", "PROPAGATION_REQUIRED,-Exception,readOnly");
//        properties.setProperty("add*", "PROPAGATION_REQUIRED,-Exception,readOnly");
//        properties.setProperty("save*", "PROPAGATION_REQUIRED,-Exception,readOnly");
//        properties.setProperty("update*", "PROPAGATION_REQUIRED,-Exception,readOnly");
//        properties.setProperty("delete*", "PROPAGATION_REQUIRED,-Exception,readOnly");
//        TransactionInterceptor tsi = new TransactionInterceptor(transactionManager,properties);
//        return tsi;
//    }
//    @Bean
//    public BeanNameAutoProxyCreator txProxy() {
//        BeanNameAutoProxyCreator creator = new BeanNameAutoProxyCreator();
//        creator.setInterceptorNames("txAdvice");
//        creator.setBeanNames("*Service", "*ServiceImpl");
//        creator.setProxyTargetClass(true);
//        return creator;
//    }
}
