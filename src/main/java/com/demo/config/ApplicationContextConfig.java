package com.demo.config;


import com.demo.Application;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@ImportResource("classpath:transaction.xml")
@SpringBootApplication(scanBasePackageClasses = Application.class)
@EnableTransactionManagement
public class ApplicationContextConfig {

    @RequestMapping("/")
    public String home() {
        return "redirect:/hello/index";
    }

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return new EmbeddedServletContainerCustomizer() {
            @Override
            public void customize(ConfigurableEmbeddedServletContainer configurableEmbeddedServletContainer) {
                ErrorPage error401Page = new ErrorPage(HttpStatus.UNAUTHORIZED, "/webapp/static/pages/error/401.html");
                ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/webapp/static/pages/error/404.html");
                ErrorPage error500Page = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/webapp/static/pages/error/500.html");
                configurableEmbeddedServletContainer.addErrorPages(error401Page, error404Page, error500Page);
//                configurableEmbeddedServletContainer.setContextPath("/test");
            }
        };
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(ApplicationContextConfig.class, args);
    }

    @Bean
    public Object testBean(PlatformTransactionManager platformTransactionManager){
        System.out.println(">>>>>>>>>>" + platformTransactionManager.getClass().getName());
        return new Object();
    }
}
