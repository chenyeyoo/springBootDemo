package com.demo.config;

import java.lang.annotation.*;

/**
 * @Package: [com.demo.config]
 * @Description: [一句话描述该类的功能]
 * @Author: [chenye]
 * @CreateDate: [2018/3/8 9:25]
 * @UpdateUser: [chenye(如多次修改保留历史记录，增加修改记录)]
 * @UpdateDate: [2018/3/8 9:25，(如多次修改保留历史记录，增加修改记录)]
 * @UpdateRemark: [说明本次修改内容, (如多次修改保留历史记录，增加修改记录)]
 * @Version: [v1.0]
 */
@Target(ElementType.METHOD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface Test {
}
