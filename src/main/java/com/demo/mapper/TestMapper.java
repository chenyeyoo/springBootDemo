package com.demo.mapper;

import com.demo.domain.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Package: [com.demo.mapper]
 * @Description: [一句话描述该类的功能]
 * @Author: [chenye]
 * @CreateDate: [2018/3/9 11:09]
 * @UpdateUser: [chenye(如多次修改保留历史记录，增加修改记录)]
 * @UpdateDate: [2018/3/9 11:09，(如多次修改保留历史记录，增加修改记录)]
 * @UpdateRemark: [说明本次修改内容, (如多次修改保留历史记录，增加修改记录)]
 * @Version: [v1.0]
 */
public interface TestMapper {
    List<User> selectByName(@Param("name")String name);
    void insert(User user);
}
